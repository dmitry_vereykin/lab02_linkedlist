//----------------------------------------------------------------------
// LinkedStringLog.java       by Dale/Joyce/Weems              Chapter 2
//
// Implements StringLogInterface using a linked list 
// of LLStringNode to hold the log strings.
//----------------------------------------------------------------------
/**
 * Edited by Dmitry Vereykin on 9/15/2015.
 */


public class LinkedStringLog implements StringLogInterface 
{
  protected LLStringNode log; // reference to first node of linked 
                              // list that holds the StringLog strings
  protected String name;      // name of this StringLog
  
  public LinkedStringLog(String name)
  // Instantiates and returns a reference to an empty StringLog object 
  // with name "name".
  {
    log = null;
    this.name = name;
  }

  public void insert(String element)
  // Precondition:   This StringLog is not full.
  //
  // Places element into this StringLog.
  {      
    LLStringNode newNode = new LLStringNode(element);
    newNode.setLink(log);
    log = newNode;
  }

  public boolean isFull()
  // Returns true if this StringLog is full, false otherwise.
  {              
    return false;
  }

  public boolean isEmpty() {
    if (log == null) {
      return  true;
    }
    return false;
  }
  
  public int size()
  // Returns the number of Strings in this StringLog.
  {
    int count = 0;
    LLStringNode node;
    node = log;
    while (node != null)
    {
      count++;
      node = node.getLink();
    }
    return count;
  }
  
  public boolean contains(String element)
  // Returns true if element is in this StringLog,
  // otherwise returns false.
  // Ignores case difference when doing string comparison.
  {                 
    LLStringNode node;
    node = log;

    while (node != null) 
    {
      if (element.equalsIgnoreCase(node.getInfo()))  // if they match
        return true;
      else
        node = node.getLink();
    }

   return false;
  }

  public int howMany(String element) {
    int count = 0;
    LLStringNode node;
    node = log;

    while (node != null)
    {
      if (element.equalsIgnoreCase(node.getInfo()))
        count++;
        node = node.getLink();
    }

   return count;
  }

  public boolean uniqueInsert(String element) {
    boolean flag = true;
    LLStringNode node;
    node = log;

    while (node != null)
    {
      if (element.equalsIgnoreCase(node.getInfo())) {
        flag = false;
        break;
      }

      node = node.getLink();
    }

    if (!flag) {
      insert(element);
    }

    return flag;
  }

  public String smallest() {
    LLStringNode node;
    node = log;
    String smallest = node.getInfo();

    while (node != null)
    {
      if (node.getInfo().compareTo(smallest) < 0)
        smallest = node.getInfo();
      node = node.getLink();
    }

    return smallest;
  }
  
  public void clear()
  // Makes this StringLog empty.
  { 
    log = null;
  }

  public String getName()
  // Returns the name of this StringLog.
  {
    return name;
  }

  public String toString()
  // Returns a nicely formatted string representing this StringLog.
  {
    String logString = "Log: " + name + "\n\n";
    LLStringNode node;
    node = log;
    int count = 0;
    
    while (node != null)
    {
      count++;
      logString = logString + count + ". " + node.getInfo() + "\n";
      node = node.getLink();
    }


    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>" + log.getLink());
    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>" + log.getInfo());

    return logString;
  }
}