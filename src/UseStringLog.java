//----------------------------------------------------------------------------
// UseStringLog.java           by Dale/Joyce/Weems                   Chapter 2
//
// Simple example of the use of a StringLog.
//----------------------------------------------------------------------------
/**
 * Edited by Dmitry Vereykin on 9/15/2015.
 */


public class UseStringLog {
    public static void main(String[] args) {
        StringLogInterface sample;
        sample = new LinkedStringLog("Example #1");
        sample.insert("Elvis");
        sample.insert("King Louis XII");
        sample.insert("Captain Kirk");
        System.out.println(sample);
        System.out.println("The size of the log is " + sample.size());
        System.out.println("Elvis is in the log: " + sample.contains("Elvis"));
        System.out.println("Santa is in the log: " + sample.contains("Santa"));

        System.out.println("\n");

        StringLogInterface sample2;
        sample2 = new LinkedStringLog("Example #2");
        sample2.insert("Mr Nobody");
        sample2.insert("Alice");
        sample2.insert("Captain Sparrow");
        sample2.insert("Dinosaurs");
        System.out.println(sample2);
        System.out.println("Insert Alice?: " + sample2.uniqueInsert("Alice"));
        System.out.println("The name of the log: " + sample2.getName());
        System.out.println("The size of the log is " + sample2.size() + "\n");
        System.out.println("The log is full: " + sample2.isFull());
        System.out.println("The log is empty: " + sample2.isEmpty() + "\n");
        System.out.println("Alice is in the log: " + sample2.contains("Alice"));
        System.out.println("Gluten is in the log: " + sample2.contains("Gluten"));
        System.out.println("The smallest: " + sample2.smallest() + "\n");
        sample2.clear();

        if (sample2.size() == 0) {
            System.out.println("The log was cleared.\n\n");
        }



        StringLogInterface sample3;
        sample3 = new LinkedStringLog("Example #3");
        System.out.print(sample3);
        System.out.println("The log is full: " + sample2.isFull());
        System.out.println("The log is empty: " + sample3.isEmpty() + "\n");


        StringLogInterface sample4;
        sample4 = new LinkedStringLog("Example #4");
        sample4.insert("Mr Smith");
        sample4.insert("Neo");
        sample4.insert("Mr Smith");
        sample4.insert("Dinosaurs");
        sample4.insert("Mr Smith");
        System.out.println(sample4);
        System.out.println("Insert Alice?: " + sample4.uniqueInsert("Alice"));
        System.out.println("The log is full: " + sample4.isFull());
        System.out.println("The log is empty: " + sample4.isEmpty() + "\n");
        System.out.println("Mr Smith is in the log " + sample4.howMany("Mr Smith") + " times.");
        System.out.println("The smallest: " + sample4.smallest());
    }
}